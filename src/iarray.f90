MODULE iarray
!
!  Contains all array var names.
!
    implicit none

    real, allocatable :: xface(:), yface(:), zface(:)
    real, allocatable :: rhokap(:,:,:)
    real, allocatable :: jmean(:,:,:), jmeanGLOBAL(:,:,:), albedoar(:,:,:), hggar(:,:,:)
    real, allocatable :: jmeanGLOBAL_on(:,:,:)
    real, allocatable :: refrac(:,:,:)

    real,allocatable :: grey_matter(:,:,:), white_matter(:,:,:), glial_matter(:,:,:)
    real, allocatable :: tumour(:,:,:), tumour_killed(:,:,:), tumour_resec(:,:,:)
    real, allocatable:: balloon(:,:,:), blob(:,:,:), blob_killed(:,:,:)

    real, allocatable:: S_0(:,:,:), O2_3(:,:,:), O2_1(:,:,:)

    real, allocatable:: S0_slice(:,:,:), o23_slice(:,:,:), o21_slice(:,:,:), tumkill_slice(:,:,:)

    real, allocatable:: rhokap_gbm(:,:,:), ua_ppix(:,:,:), albedoar_gbm(:,:,:), ua_ar(:,:,:)

    real, allocatable:: power(:), percent_left(:)
     
end MODULE iarray
