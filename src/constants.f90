MODULE constants
!
! Module containing constants:
!         PI,TWOPI, the number of grid elements in each direction n%g,
!         the various bin # params,
!         and the vars for the filepaths.
!

    implicit none
    save
    
    integer, parameter :: treatment_time=515 !8.6 min
    !integer, parameter :: treatment_time=1032 !17.2 min
   ! integer, parameter :: treatment_time=558 !9.3 min
    !integer, parameter :: treatment_time= 1116 !18.6 min
    !integer, parameter :: treatment_time=1800 !30 min
    !integer, parameter :: treatment_time=660 !11 min
    !integer, parameter :: treatment_time=270 !4.5 min
    !integer, parameter :: treatment_time=330 !5.5 min

    integer, parameter :: start_time=1 , end_time= treatment_time + (4*120)! treatment time plus 4 2 minute fractionation breaks
    integer, parameter :: nxg=250, nyg=231, nzg=155
    real,    parameter :: PI=4.*atan(1.), TWOPI=2.*PI, OFFSET=1.e-2*(2.*.5/nxg)
    real               :: xmax, ymax, zmax, v(3), costim, sintim, cospim, sinpim
    character(len=255) :: cwd, homedir, fileplace, resdir

    !oxygen calcs
    real, parameter :: epsil= 3.7E-3, sig=9E-5, beta=11.9, del=33.d0
    real, parameter :: gg=21.6, o2_3_t0=38.d0

end MODULE constants