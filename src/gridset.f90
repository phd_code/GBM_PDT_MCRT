MODULE gridset_mod

    implicit none

    private
    public :: gridset

    contains
        subroutine gridset(id,ts)

            use constants, only : nxg, nyg, nzg, xmax, ymax, zmax
            use iarray
            use opt_prop
            use ch_opt, only : init_opt_630

            implicit none

            integer, intent(IN) :: id,ts

            integer             :: i, j, k
            real                :: x, y, z, taueq1, taupole1, taueq2, taupole2

            real :: rad_small, rad_blob, a, b, c

            if(id == 0)then
                print*, ' '
                print *, 'Setting up density grid....'
            end if

            ! setup grid faces
            do i = 1, nxg + 1
                xface(i) = (i - 1) * 2. * xmax/nxg
            end do

            do i = 1, nyg + 1
                yface(i) = (i - 1) * 2. * ymax/nyg
            end do

            do i = 1, nzg + 2
                zface(i) = (i - 1) * 2. * zmax/nzg
            end do

            call init_opt_630
            refrac(:,:,:) = n1

            !set up optical properties grid
            do i = 1, nxg
                x = xface(i) - xmax + xmax/nxg
                do j = 1, nyg
                    y = yface(j) - ymax + ymax/nyg
                    do k = 1, nzg
                        z = zface(k) - zmax + zmax/nzg
                        !rhokap(i,j,k) = kappa
                        !refrac(i,j,k) = n2


                        

                        !set tumour sticking out of brain due to balloon algorithm to zeros
                        if(white_matter(i,j,k) .eq. 0. .and. grey_matter(i,j,k) .eq. 0. &
                        .and. glial_matter(i,j,k) .eq. 0.)then
                          !tumour(i,j,k)=0.
                          if(tumour(i,j,k) .ne. 1.)then
                          tumour_killed(i,j,k)=0.
                          tumour_resec(i,j,k)=0.
                        endif
                      endif

                      a=4.0/2.
                      b=7.0/2.
                      c=3.0/2.
                   
                   
                      

                      if(white_matter(i,j,k) .ne. 0. .or. grey_matter(i,j,k) .ne. 0. &
                  .or. glial_matter(i,j,k) .ne. 0 .or. tumour_resec(i,j,k) .ne. 0.)then

                    refrac(i,j,k)=n2

                  rhokap(i,j,k)=((1-tumour_resec(i,j,k))*(white_matter(i,j,k)/(white_matter(i,j,k)&
                  +grey_matter(i,j,k)+glial_matter(i,j,k))))*whitematter_kappa &
                  +((1-tumour_resec(i,j,k))*(grey_matter(i,j,k)/(white_matter(i,j,k)&
                  +grey_matter(i,j,k)+glial_matter(i,j,k))))*greymatter_kappa &
                  + ((1-tumour_resec(i,j,k))*(glial_matter(i,j,k)/(white_matter(i,j,k)&
                  +grey_matter(i,j,k)+glial_matter(i,j,k))))*glial_kappa &
                  + tumour_resec(i,j,k)*rhokap_gbm(i,j,k)

                  albedoar(i,j,k)=((1-tumour_resec(i,j,k))*(white_matter(i,j,k)/(white_matter(i,j,k)&
                  +grey_matter(i,j,k)+glial_matter(i,j,k))))*whitematter_albedo &
                  +((1-tumour_resec(i,j,k))*(grey_matter(i,j,k)/(white_matter(i,j,k)&
                  +grey_matter(i,j,k)+glial_matter(i,j,k))))*greymatter_albedo &
                  + ((1-tumour_resec(i,j,k))*(glial_matter(i,j,k)/(white_matter(i,j,k)&
                  +grey_matter(i,j,k)+glial_matter(i,j,k))))*glial_albedo &
                  + tumour_resec(i,j,k)* albedoar_gbm(i,j,k)

                  hggar(i,j,k)=((1-tumour_resec(i,j,k))*(white_matter(i,j,k)/(white_matter(i,j,k)&
                  +grey_matter(i,j,k)+glial_matter(i,j,k))))*whitematter_hgg &
                  +((1-tumour_resec(i,j,k))*(grey_matter(i,j,k)/(white_matter(i,j,k)&
                  +grey_matter(i,j,k)+glial_matter(i,j,k))))*greymatter_hgg &
                  + ((1-tumour_resec(i,j,k))*(glial_matter(i,j,k)/(white_matter(i,j,k) &
                  +grey_matter(i,j,k)+glial_matter(i,j,k))))*glial_hgg &
                  + tumour_resec(i,j,k)* gbm_hgg

                  ua_ar(i,j,k)=((1-tumour_resec(i,j,k))*(white_matter(i,j,k)/(white_matter(i,j,k)&
                  +grey_matter(i,j,k)+glial_matter(i,j,k))))*whitematter_mua &
                  +((1-tumour_resec(i,j,k))*(grey_matter(i,j,k)/(white_matter(i,j,k)&
                  +grey_matter(i,j,k)+glial_matter(i,j,k))))*greymatter_mua &
                  + ((1-tumour_resec(i,j,k))*(glial_matter(i,j,k)/(white_matter(i,j,k) &
                  +grey_matter(i,j,k)+glial_matter(i,j,k))))*glial_mua &
                  + tumour_resec(i,j,k)* gbm_mua


                endif


                ! insert space to clear brain
                rad_small=1.6
                if(sqrt((x+0.7)**2+(y+2.7)**2+(z)**2) .le. sqrt(rad_small**2)) then

                    rhokap(i,j,k)=0.
                    albedoar(i,j,k)=0.
                    tumour_resec(i,j,k)=0.
                    tumour_killed(i,j,k)=0.
                    white_matter(i,j,k)=0.
                    grey_matter(i,j,k)=0.
                    ua_ar(i,j,k)=0.
 
                    refrac(i,j,k)=n1
                    hggar(i,j,k)=0.
                  end if

                  !set resected cavity space to csf properties
                  !resected tumour has resected parts equal 1
                  if (tumour(i,j,k) .ge. 0.95)then
                    rhokap(i,j,k)=kappa_water
                    albedoar(i,j,k)=albedo_water
                    ua_ar(i,j,k)=mua_water
                    refrac(i,j,k)=1.33
                    tumour_resec(i,j,k)=0.
                    tumour_killed(i,j,k)=0.
                    white_matter(i,j,k)=0.
                    grey_matter(i,j,k)=0.
                    glial_matter(i,j,k)=0.
            
                  endif

                

                  if(balloon(i,j,k) .eq. 100.)then
                    rhokap(i,j,k)=kappa_intra
                    albedoar(i,j,k)=albedo_intra
                    ua_ar(i,j,k)=mua_intra
                    tumour_resec(i,j,k)=0.
                    tumour_killed(i,j,k)=0.
             
 
                     refrac(i,j,k)=1.33 !water refractive index
 
                     hggar(i,j,k)=hgg_intra

                  end if
 
                  rad_blob=0.2
 
                  !add in a blob
                  if(sqrt((x-2.1)**2+(y+0.7)**2+(z)**2) .le. sqrt(rad_blob**2)) then !sphere
                    blob(i,j,k)=1.
                    rhokap(i,j,k)=gbm_kappa
                    albedoar(i,j,k)=gbm_albedo
                    hggar(i,j,k)=gbm_hgg
                    ua_ar(i,j,k)=gbm_mua
                    refrac(i,j,k)=n2
                  
 
                    if(ts .eq.1)then
                      blob_killed(i,j,k) =1.
                      tumour_killed(i,j,k)=0.
                      tumour_resec(i,j,k)=0.
                    endif
                  endif

                  

                  if(ts .eq. 1)then
                    S_0(i,j,k)=5.d0*(tumour_resec(i,j,k) + blob(i,j,k))
                end if

                
                if(((x-(2.*xmax-5.9))**2/a**2) + ((y-(2.*ymax-7.3))**2/b**2) +((z-(2.*zmax-3.71))**2/c**2) .le. 1.) then

                     rhokap(i,j,k)=kappa_intra
                      albedoar(i,j,k)=albedo_intra
                     white_matter(i,j,k)=0.
                     grey_matter(i,j,k)=0.
                     tumour_resec(i,j,k)=0.
                     tumour_killed(i,j,k)=0.
                     ua_ar(i,j,k)=mua_intra
           
                     refrac(i,j,k)=1.33 !water refractive index
           
                     hggar(i,j,k)=hgg_intra
                  endif
                !endif


                      !uncomment for Jacques plot
                        !rhokap(i,j,k)= 0.23 + 21.d0/(1-0.9)
                        !albedoar(i,j,k)=  (21.d0/(1-0.9))/rhokap(i,j,k)
                        !hggar(i,j,k)=0.9
                        !refrac(i,j,k)=n2

                     !uncomment for Lille calf brain test

                        !rhokap(i,j,k)= 0.19 + 20.09/(1-0.85)
                        !albedoar(i,j,k)=  (20.09/(1-0.85))/rhokap(i,j,k)
                       ! hggar(i,j,k)=0.85
                       ! refrac(i,j,k)=n2


                        !volume = 54.57 cm3
                        !time = 9.85 mins
                       ! a=4.1/2.
                       ! b=6.2/2.
                       ! c=4.1/2.
            

                        ! if(((x)**2/a**2) + ((y)**2/b**2) +((z)**2/c**2) .le. 1.) then
                            ! values from van gemert paper - assuming lille intralipid is a 0.2%
                         !   rhokap(i,j,k)=kappa_intra
                          !  albedoar(i,j,k)=albedo_intra
            
                           !  refrac(i,j,k)=1.33 !water refractive index
            
                            ! hggar(i,j,k)=hgg_intra
                              !print*, 'done'
                              !hgg=0.733 of intralipid at 635nm from p106 lille thesis
                        !  end if  

                

                    end do
                end do
            end do

            !****************** Calculate equatorial and polar optical depths ****
            taueq1   = 0.
            taupole1 = 0.
            taueq2   = 0.
            taupole2 = 0.

            do i = 1, nxg
                taueq1 = taueq1 + rhokap(i,nyg/2,nzg/2)
            end do

            do i = 1, nzg
                taupole1 = taupole1 + rhokap(nxg/2,nyg/2,i)
            end do

            taueq1 = taueq1 * 2. * xmax/nxg
            taupole1 = taupole1 * 2. * zmax/nzg
            if(id == 0)then
                print'(A,F9.5,A,F9.5)',' taueq1 = ',taueq1,'  taupole1 = ',taupole1
            end if

            if(id == 0)then
                inquire(iolength=i)refrac(:,:,:nzg)
                open(newunit=j,file='refrac.dat',access='stream',form='unformatted',status='replace')
                write(j)refrac(:,:,:)
                close(j)
            end if
            ! call mpi_finalize()
            ! stop
        end subroutine gridset
end MODULE gridset_mod
