MODULE subs

implicit none

    contains

        subroutine directory
        !  subroutine defines vars to hold paths to various folders   
        !   
        !   
            use constants, only : cwd, homedir, fileplace, resdir

            implicit none

            !get current working directory

            call get_environment_variable('PWD', cwd)

            ! get 'home' dir from cwd
            homedir = trim(cwd(1:len(trim(cwd))-3))
            ! get data dir
            fileplace = trim(homedir)//'data/'
            ! get res dir
            resdir = trim(homedir)//'res/'

        end subroutine directory


        subroutine zarray

            use iarray

            !sets all arrays to zero
            implicit none


            jmean = 0.
            xface = 0.
            yface = 0.
            zface = 0.
            rhokap = 0.
            jmeanGLOBAL = 0.
            jmeanGLOBAL_on=0.
            albedoar=0.
            hggar=0.
            refrac = 0.

            grey_matter=0.
            white_matter=0.
            glial_matter=0.

            tumour=0.
            tumour_killed=0.
            tumour_resec=0.
            balloon=0.
            blob=0.
            blob_killed=0.

            S_0=0.
            O2_3=0.

            S0_slice=0.
            o23_slice=0.
            o21_slice=0.
            tumkill_slice=0.

            rhokap_gbm=0.
            albedoar_gbm=0.
            ua_ppix=0.
            ua_ar=0.

            power=0.
            percent_left=0.
        end subroutine zarray


        subroutine alloc_array
        !  subroutine allocates allocatable arrays
        !   
        !   
            use iarray
            use constants,only : nxg,nyg,nzg, start_time, end_time

            implicit none

            allocate(xface(nxg+1), yface(nyg + 1), zface(nzg + 2))
            allocate(rhokap(nxg, nyg, nzg+1))
            allocate(jmean(nxg, nyg, nzg+1), jmeanGLOBAL(nxg, nyg, nzg+1))
            allocate(jmeanGLOBAL_on(nxg, nyg, nzg+1))
            allocate(albedoar(nxg, nyg, nzg+1), hggar(nxg, nyg, nzg+1))
            allocate(refrac(nxg, nyg, nzg + 1))

            allocate(white_matter(nxg,nyg,nzg+1),grey_matter(nxg,nyg,nzg+1))
            allocate(glial_matter(nxg,nyg,nzg+1),tumour(nxg,nyg,nzg+1))
            allocate(tumour_killed(nxg,nyg,nzg+1),tumour_resec(nxg,nyg,nzg+1))
            allocate(balloon(nxg,nyg,nzg+1), blob(nxg,nyg,nzg+1),blob_killed(nxg,nyg,nzg+1) )

            allocate(S_0(nxg,nyg,nzg+1), O2_3(nxg,nyg,nzg+1),O2_1(nxg,nyg,nzg+1))

            allocate(S0_slice(nxg,nyg,(end_time-start_time)+1), o23_slice(nxg,nyg,(end_time-start_time)+1))
            allocate(o21_slice(nxg,nyg,(end_time-start_time)+1),tumkill_slice(nxg,nyg,(end_time-start_time)+1))

            allocate(rhokap_gbm(nxg,nyg,nzg+1), ua_ppix(nxg,nyg,nzg+1), albedoar_gbm(nxg,nyg,nzg+1),ua_ar(nxg,nyg,nzg+1))

            allocate(power(end_time-start_time+1),percent_left(end_time-start_time+1))
          
        end subroutine alloc_array


        subroutine ppix_con(ts)

            !calculates conentrarion of ppix at the start of each time step
            
                  use iarray
                  use constants
                  use opt_prop
            
                  implicit none
        
                    integer, intent(in) :: ts
                    integer :: i,j,k
                             
                  do k = 1, nzg
                      do j = 1, nyg
                          do i = 1, nxg
            
                            if(k .eq. 78)then
                              S0_slice(i,j,ts)=S_0(i,j,k)
                            endif
            
                       if (rhokap(i,j,k) .ne. 0 .and. rhokap(i,j,k) .ne. kappa_water .and. rhokap(i,j,k) .ne. kappa_intra)then
                           if(tumour_resec(i,j,k) + blob(i,j,k) .eq. 0.)then
                             S_0(i,j,k)=0.1 !need to look at literature for this value
                           else
                             S_0(i,j,k)= S_0(i,j,k)+ ((-epsil*sig*jmeanGLOBAL(i,j,k)*(S_0(i,j,k)+del)*O2_3(i,j,k)&
                                                      /(O2_3(i,j,k)+beta))*S_0(i,j,k))
                          endif
                        endif
            
            
                            if(S_0(i,j,k).lt. 0.)then
                              S_0(i,j,k)=0.
                            endif
            
            
            
                          end do
                      end do
                  end do
            
            
            
        end subroutine ppix_con

        subroutine trip_ox(ts) !changes triplet oxygen concentration with time

            !calculates conentrarion of ppix at the start of each time step
            
                  use iarray
                  use constants
                  use opt_prop
            
                  implicit none
            
                    integer, intent(IN)::ts
            
                    real :: gtd, td
                    integer :: i,j,k
            
            
                  !gg time dependance calculation from doi: 10.1117/12.2211120
                  td=(ts-750)/632.1
            
                  gtd=gg*((0.99*td**4+1.09*td**3+0.05*td**2+0.18*td+0.32)&
                           /(td**4+1.16*td**3+0.18*td**2+0.24*td +0.31))
            
            
                  do k = 1, nzg
                      do j = 1, nyg
                          do i = 1, nxg
            
                            if(k .eq. 78)then
                              o23_slice(i,j,ts)=O2_3(i,j,k)
                            endif
           
                           if (rhokap(i,j,k) .eq. 0 .or. rhokap(i,j,k) .eq. kappa_water .or. rhokap(i,j,k) .eq. kappa_intra)then
                             O2_3(i,j,k)=0.
                           else
                           O2_3(i,j,k)=O2_3(i,j,k) +gtd*(1-(O2_3(i,j,k)/o2_3_t0)) &
                                                  - (epsil*jmeanGLOBAL(i,j,k)*S_0(i,j,k)/(O2_3(i,j,k)+beta))*O2_3(i,j,k)
            
                            endif
            
                          if(O2_3(i,j,k) .lt. 0.)then
                            O2_3(i,j,k)=0.
                          end if
            
                          end do
                      end do
                  end do
            
            
        end subroutine trip_ox

        subroutine sing_ox(ts) !changes triplet oxygen concentration with time

            !calculates conentrarion of ppix at the start of each time step
            
              use iarray
              use constants
              use opt_prop
            
              implicit none
            
                integer, intent(IN):: ts
            
                integer :: i,j,k
            
            
            
              do k = 1, nzg
                  do j = 1, nyg
                      do i = 1, nxg
            
                        if(k .eq. 78)then
                          o21_slice(i,j,ts)=O2_1(i,j,k)
                        endif

                       if (rhokap(i,j,k) .eq. 0 .or. rhokap(i,j,k) .eq. kappa_water .or. rhokap(i,j,k) .eq. kappa_intra)then
                         O2_1(i,j,k)=0.
                       else
                       O2_1(i,j,k)=O2_1(i,j,k) + ((epsil*jmeanGLOBAL(i,j,k)*S_0(i,j,k)*O2_3(i,j,k))/(O2_3(i,j,k)+beta))
                      endif
            
            
                       if(O2_1(i,j,k) .lt. 0.)then
                         O2_1(i,j,k)=0.
                       end if
            
            
            
                      end do
                  end do
              end do
            
            
        end subroutine sing_ox

        subroutine cell_kill(ts) !changes triplet oxygen concentration with time

          !calculates conentrarion of ppix at the start of each time step
          
            use iarray
            use constants, only : nxg, nyg, nzg,xmax,ymax,zmax
          
            implicit none
          
            integer, intent(IN):: ts
          
              integer :: i,j,k
          
          
          
            do k = 1, nzg
                do j = 1, nyg
                    do i = 1, nxg
          
                      if(k .eq. 78)then
                        tumkill_slice(i,j,ts)=tumour_killed(i,j,k) + blob_killed(i,j,k)
                      endif
                  if(O2_1(i,j,k) .ge. 560)then
                  !if(O2_1(i,j,k) .ge. 1000.)then
                    tumour_killed(i,j,k)=0.
                    blob_killed(i,j,k)=0.
                    !balloon_killed(i,j,k)=0.
                  end if
          
          
          
                    end do
                end do
            end do
          
          
          end subroutine cell_kill

          subroutine percent_killed_calc(ts)

            !calculates percentage of tumour left at end of time step
            
              use iarray, only: tumour_killed, tumour_resec, percent_left, blob, blob_killed
              use constants, only: nxg,nyg,nzg
            
            
              implicit none
            
              integer, intent(IN):: ts
            
              integer :: i,j,k
              real :: per_kill, per_left
            
              per_left= (sum(tumour_killed+blob_killed)/sum(tumour_resec+blob))*100.
            !  per_left= (sum(balloon_killed)/sum(balloon))*100.
              per_kill = 100 - per_left
            
              percent_left(ts)=per_left
            
            end subroutine percent_killed_calc
            

                
end MODULE subs