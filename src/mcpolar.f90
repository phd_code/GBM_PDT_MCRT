program mcpolar

 use mpi

!shared data
use constants
use photon_vars
use iarray
use opt_prop

!subroutines
use subs
use gridset_mod
use sourceph_mod
use inttau2
use ch_opt
use stokes_mod
use writer_mod

implicit none

integer          :: nphotons, iseed, j, xcell, ycell, zcell, ts
logical          :: tflag
double precision :: nscatt
real             :: ran, delta, start,finish,ran2

integer :: id, error, numproc,u
real    :: nscattGLOBAL, po, frac, rest


call cpu_time(start)

!set directory paths
call directory

!allocate and set arrays to 0
call alloc_array
call zarray

!init MPI
 call MPI_init(error)
 call MPI_Comm_size(MPI_COMM_WORLD, numproc, error)
 call MPI_Comm_rank(MPI_COMM_WORLD, id, error)
!id = 0
!numproc = 1

!**** Read in parameters from the file input.params
open(10,file=trim(resdir)//'input.params',status='old')
   read(10,*) nphotons
   read(10,*) xmax
   read(10,*) ymax
   read(10,*) zmax
   read(10,*) n1
   read(10,*) n2
   close(10)
   !****************** Read in brain tumour data grids*************************************
   open(newunit=u,file='grey_hires2.txt',status='old')!status='old'))
   read(u,*,end=102) grey_matter
   102 close(u)

   open(newunit=u,file='white_hires2.txt',status='old')
   read(u,*,end=101) white_matter
   101 close(u)

   open(newunit=u,file='glial_hires2.txt',status='old')
   read(u,*,end=106) glial_matter
   106 close(u)

   open(newunit=u,file='tum_hires2.txt',status='old')
   read(u,*,end=103) tumour
   103 close(u)

   open(newunit=u,file='tum_hires2.txt',status='old')
   read(u,*,end=104) tumour_killed
   104 close(u)

   open(newunit=u,file='tum_hires2.txt',status='old')
   read(u,*,end=105) tumour_resec
   105 close(u)


   open(newunit=u,file='balloon_hires2.txt',status='old')
   read(u,*,end=107) balloon
   107 close(u)

!set initial values
O2_1=0.
O2_3= 38.

do ts= start_time, end_time !start time step
! set seed for rnd generator. id to change seed for each process
iseed=-95648324+id + ts
iseed=-abs(iseed)  ! Random number seed must be negative for ran2

if(ts .eq. start_time)then !use to only run MCRT once as fluence unlinkly to change much - major time save
call init_opt_630

if(id == 0)then
   print*, ''
   print*,'# of photons to run',nphotons*numproc
end if


!***** Set up density grid *******************************************
call gridset(id,ts)

!***** Set small distance for use in optical depth integration routines
!***** for roundoff effects when crossing cell walls
delta = 1.e-8*(2.*zmax/nzg)
nscatt=0

! call cpu_time(start)

!loop over photons
print*,'Photons now running on core: ',id

do j = 1, nphotons

   tflag=.FALSE.

   if(mod(j,10000) == 0)then
      print *, j,' scattered photons completed on core: ',id
   end if

!***** Release photon from point source *******************************
   !call evenDis(xcell,ycell,zcell,iseed) !Jacques
   call fibre_lille(xcell,ycell,zcell,iseed) !Lille calf
!****** Find scattering location

      call tauint1(xcell,ycell,zcell,tflag,iseed,delta)
!******** Photon scatters in grid until it exits (tflag=TRUE)
      do while(tflag.eqv..FALSE.)

         ran = ran2(iseed)
         if(ran < albedoar(xcell,ycell,zcell))then!interacts with tissue
               call stokes(iseed, xcell, ycell, zcell)
               nscatt = nscatt + 1
            else
               tflag=.true.
               exit
         end if

!************ Find next scattering location

         call tauint1(xcell,ycell,zcell,tflag,iseed,delta)

      end do
end do      ! end loop over nph photons


!jmeanGLOBAL = jmean
!nscattGLOBAL = nscatt

! collate fluence from all processes
 call mpi_reduce(jmean, jmeanGLOBAL, (nxg*nyg*nzg),MPI_DOUBLE_PRECISION, MPI_SUM,0,MPI_COMM_WORLD,error)
 call mpi_reduce(nscatt,nscattGLOBAL,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,error)


end if !for only running MCRT part once at start time

!set jme - fluence for this time step with fractionation - 5 fractions with 2 min rest
frac=103 ! 8.6 min treatment
!frac= 206 ! 17.2 minute treatment
!frac=112 !9.3 min treatment
!frac = 223 ! 18.6 min treatment
!frac=360 !30 min treatment
!frac= 132 !11 min treatment
!frac=90 !4.5 min treatment over 3 fractions
!frac=33 ! 5.5 min over 10 fractions
rest=120 


          if((0 < ts .and. ts < frac) .or. (frac + rest < ts .and. ts < (2*frac)+rest) &
         .or. ((2*frac)+(2*rest)<ts .and. ts <(3*frac)+(2*rest)) &
         .or. ((3*frac)+(3*rest) <ts .and. ts < (4*frac)+(3*rest)).or. &
         ((4*frac)+(4*rest) <ts .and. ts < (5*frac)+(4*rest)) ) then 
        ! ((5*frac)+(5*rest) < ts .and. ts < (6*frac) +(5*rest)) .or. &
         
         !   ((6*frac)+(6*rest) < ts .and. ts < (7*frac) +(6*rest))&
         !.or. ((7*frac)+(7*rest)<ts .and. ts <(8*frac)+(7*rest)) &
        ! .or. ((8*frac)+(8*rest) <ts .and. ts < (9*frac)+(8*rest)).or. &
         !((9*frac)+(9*rest) <ts .and. ts <= (10*frac)+(9*rest))) then 


           po=2000.d0
          
           ! jmeanGLOBAL =jmeanGLOBAL * ((2.*xmax)*(2.*ymax)/(ts*nphotons*numproc*(2.*xmax/nxg)*(2.*ymax/nyg)*(2.*zmax/nzg))) !jacques
           if(ts .eq. 1)then
           jmeanGLOBAL =jmeanGLOBAL * (po/(nphotons*numproc*(2.*xmax/nxg)*(2.*ymax/nyg)*(2.*zmax/nzg))) !use for brain fibre
           !jmeanGLOBAL =jmeanGLOBAL * ((2.*xmax)*(2.*ymax)/(nphotons*numproc*(2.*xmax/nxg)*(2.*ymax/nyg)*(2.*zmax/nzg))) !run if running MCRT onle once is on -jacques
           jmeanGLOBAL_on = jmeanGLOBAL
           end if
          jmeanGLOBAL=jmeanGLOBAL_on
           power(ts)=po

         else !turn light off

          po=0. !fractionation
          jmeanGLOBAL=0. !fractionation
         !  po=2000.d0 !no fractionation
           if(ts .eq. 1)then
         !   jmeanGLOBAL =jmeanGLOBAL * (po/(nphotons*numproc*(2.*xmax/nxg)*(2.*ymax/nyg)*(2.*zmax/nzg))) !use for brain fibre
            jmeanGLOBAL =jmeanGLOBAL * ((2.*xmax)*(2.*ymax)/(nphotons*numproc*(2.*xmax/nxg)*(2.*ymax/nyg)*(2.*zmax/nzg))) !run if running MCRT onle once is on
            end if
          ! jmeanGLOBAL=jmeanGLOBAL_on !no fractionation
           power(ts)=po

         
         endif 

call ppix_con(ts) !calculate new PpIX concentration
call trip_ox(ts) !calculate new triplet oxygen concentration
call sing_ox(ts) !calculate new singlet oxygen concentration
call cell_kill(ts) !kill cells over threshold
call percent_killed_calc(ts) !calculate the percentage of tumour cells killed

if(id == 0)then
   print*,'Average # of scatters per photon:',nscattGLOBAL/(nphotons*numproc)
   print*, 'Total time done:', ts
   !write out files
   call writer(xmax,ymax,zmax,nphotons, numproc, ts)
   print*,'write done'
end if

end do !end loop over time step

call cpu_time(finish)
if(finish-start.ge.60. .and. id==0)then
   print*,floor((finish-start)/60.)+mod(finish-start,60.)/100.
else
   if(id==0)print*, 'time taken ~',floor(finish-start/60.),'s'
end if

 call MPI_Finalize(error)
end program mcpolar
