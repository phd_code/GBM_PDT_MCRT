module sourceph_mod

    implicit none

    contains
        subroutine sourceph(xcell, ycell, zcell, iseed)
        ! get intial photon position


            use constants, only : nxg, nyg, nzg, xmax, ymax, zmax
            use photon_vars

            implicit none


            integer, intent(OUT)   :: xcell, ycell, zcell
            integer, intent(INOUT) :: iseed
            real                   :: ran2

            zp = zmax - epsilon(1.d0)
            xp = 2.*xmax*ran2(iseed)-xmax!ranu(-xmax, xmax, iseed)
            yp = 2.*xmax*ran2(iseed)-xmax!ranu(-ymax, ymax, iseed)

            phi = 0.
            cosp = 0.d0
            sinp = 0.d0
            cost = -1.d0
            sint =  0.d0

            nxp = sint * cosp
            nyp = sint * sinp
            nzp = cost

            !*************** Linear Grid *************************
            xcell=int(nxg*(xp+xmax)/(2.*xmax))+1
            ycell=int(nyg*(yp+ymax)/(2.*ymax))+1
            zcell=int(nzg*(zp+zmax)/(2.*zmax))+1
        !*****************************************************
        end subroutine sourceph

        subroutine evenDis(xcell, ycell, zcell, iseed)
    ! Emits photons evenly across the top of the grid

    use constants, only : nxg, nyg, nzg, xmax, ymax, zmax, TWOPI
        use photon_vars

        implicit none

        integer, intent(OUT)   :: xcell, ycell, zcell
        integer, intent(INOUT) :: iseed

        real :: ran2, theta

           !changed zp as photons were being inputted from the bottom of the grid instead of the top.
          zp = zmax + (1.d-5 * (2.d0*zmax/1190))



          if(ran2(iseed) .gt. 0.5)then
          xp=-ran2(iseed)*xmax
          else
          xp=ran2(iseed)*xmax
          end if


          if(ran2(iseed) .gt. 0.5)then
          yp=-ran2(iseed)*ymax
          else
          yp=ran2(iseed)*ymax
          end if


         phi = TWOPI * ran2(iseed)
            cosp = cos(phi)
            sinp = sin(phi)
            cost = -1.d0 !direct irradiation
       !     cost=ran2(iseed) !diffuse irradiation
            sint = sqrt(1. - cost**2)

            nxp = sint * cosp
            nyp = sint * sinp
            nzp = cost

            !*************** Linear Grid *************************
            xcell=int(nxg*(xp+xmax)/(2.*xmax))+1
            ycell=int(nyg*(yp+ymax)/(2.*ymax))+1
            zcell=int(nzg*(zp+zmax)/(2.*zmax))+1
            !*****************************************************



        end subroutine evenDis


        subroutine fibre_calf(xcell, ycell, zcell, iseed)
            ! Emits photons evenly across the top of the grid
        
                use constants, only : nxg, nyg, nzg, xmax, ymax, zmax, TWOPI, PI
                use photon_vars
        
                implicit none
        
                integer, intent(OUT)   :: xcell, ycell, zcell
                integer, intent(INOUT) :: iseed
        
                real :: ran2, theta, dd,d,rot_theta, ranran
        
        
                     rot_theta=3.*PI/4. +0.2
        
                     !dd=ran2(iseed)*25.
                     !d=(2.5/2.)-(dd/10.)
                     !xp=(2.d0*xmax) - 17.8+d*sin(rot_theta) +0.1
                     !yp=(2.d0*ymax) - 17.1 - d*cos(rot_theta) +3.5
                     !zp=zmax - 9.1
        
                     !dd=ran2(iseed)*25.
                     !d=(2.5/2.)-(dd/10.)
                     !xp=(2.d0*xmax) -( 5.46)+d*sin(rot_theta)
                     !yp=(2.d0*ymax) - (4.62) - d*cos(rot_theta)
                    !zp=2.d0*zmax - 4.13
        
                    !dd=ran2(iseed)*25.
                    !d=(2.5/2.)-(dd/10.)
                    !xp=(xmax) -( 4.06)+d*sin(rot_theta)
                    !yp=(ymax) - (2.45) - d*cos(rot_theta)
                    !zp=zmax - 6.37
        
                    dd=ran2(iseed)*5
                    d=dd/2.
        
                    ranran=ran2(iseed)
        
                    if(ranran .le. 0.5)then !see moving oxygen lab notes for new dimension calcs
                        xp=0-0.2!(xmax) !-( 1.4)!0-(2.*xmax-4.06)!0.
                        yp=0  +d!(ymax)! - (0.88) +d!(0.1-d)!-(2.*ymax-2.0)!0.1 -d
                        zp=0!zmax!-2.22!0-(2.*zmax-6.37)!0.
                      else
                        xp=0-0.2!(xmax)! -( 1.4)!0-(2.*xmax-4.06)!0.
                        yp=0-d!(ymax)! - (0.88) -d!(0.1+d)!-(2.*ymax-2.0)!0.1 +d
                        zp=0!zmax!-2.22!0-(2.*zmax-6.37)!0.
                      endif
        
                  !200ml - yp=0.9 +/- d
                  !300ml - yp=1.3+/- d
        
        
                     !xp=xmax-4.06! 5.8! 2*xmax - 17.8 = 5.8
                     !yp=ymax-2.45!3.5!2*ymax - 17.1 = 3.5
                     !zp=zmax-6.37!5.9! 2*zmax - 5.9 = 9.1
        
                     cost=2.*ran2(iseed)-1.
                     sint=(1.-cost*cost)
                     if(sint .le. 0.)then
                       sint=0.
                     else
                       sint=sqrt(sint)
                     end if
        
                     phi=TWOPI*ran2(iseed)
                     cosp=cos(phi)
                     sinp=sin(phi)
        
                     !print*, xp,yp,zp, 'source'
        
                    !if(ran2(iseed) .gt. 0.5)then
                  !    phi=(rot_theta + PI/2.d0)*ran2(iseed)
                  !  else
                  !    phi=(rot_theta - PI/2.d0)*ran2(iseed)
                  !  endif
                      ! phi =TWOPI * ran2(iseed)
                      !  cosp =cos(phi)
                      !  sinp = sin(phi)
                      !  cost = 1.d0 !direct irradiation
                      !theta= acos(2*ran2(iseed)-1.d0) !diffuse irradiation
                        !cost= ran2(iseed) !diffuse irradiation
                      !  sint = sqrt(1. - cost**2)
        
                      !cost=cos(theta)
                      !sint=sin(theta)
        
                        nxp = sint * cosp
                        nyp = sint * sinp
                        nzp = cost
        
        
                    !phi = TWOPI * ran2(iseed)
                    !theta= rot_theta+PI/2.!acos((2*ran2(iseed))-1.)
                    !cosp = cos(phi)
                    !sinp = sin(phi)
                    !cost = cos(theta)
                    !cost=ran2(iseed) !diffuse irradiation
                    !cost=-1.d0 !direct irradiation
        
                    !if(sin(theta) .le. 0.)then
                    !  sint=0.
                    !else
                    !  sint=sqrt(sin(theta))
                    !endif
                    !sint = sqrt(1. - cost**2)
        
                    !nxp = sint * cosp
                    !nyp = sint * sinp
                    !nzp = cost!cos(theta)!direct irradiation
        
                    !*************** Linear Grid *************************
                    xcell=int(nxg*(xp+xmax)/(2.*xmax))+1
                    ycell=int(nyg*(yp+ymax)/(2.*ymax))+1
                    zcell=int(nzg*(zp+zmax)/(2.*zmax))+1
                    !*****************************************************
        
        
        
                end subroutine fibre_calf

                subroutine fibre_lille(xcell, ycell, zcell, iseed)
                    ! Emits photons evenly across the top of the grid
                  
                        use constants, only : nxg, nyg, nzg, xmax, ymax, zmax, TWOPI, PI
                        use photon_vars
                  
                        implicit none
                  
                        integer, intent(OUT)   :: xcell, ycell, zcell
                        integer, intent(INOUT) :: iseed
                  
                        real :: ran2, theta, dd,d, ranran
                  
                  
                            dd=ran2(iseed)*3.9
                            d=dd/2.
                  
                            ranran=ran2(iseed)
                  
                          if(ranran .le. 0.5)then !see moving oxygen lab notes for new dimension calcs
                          xp=0.!(xmax) !-( 1.4)!0-(2.*xmax-4.06)!0.
                          yp=0 - 0.7 +d!(ymax)! - (0.88) +d!(0.1-d)!-(2.*ymax-2.0)!0.1 -d
                          zp=0!zmax!-2.22!0-(2.*zmax-6.37)!0.
                        else
                          xp=0.!(xmax)! -( 1.4)!0-(2.*xmax-4.06)!0.
                          yp=0-0.7-d!(ymax)! - (0.88) -d!(0.1+d)!-(2.*ymax-2.0)!0.1 +d
                          zp=0!zmax!-2.22!0-(2.*zmax-6.37)!0.
                        endif
                  
                  
                  
                             cost=2.*ran2(iseed)-1.
                             sint=(1.-cost*cost)
                             if(sint .le. 0.)then
                               sint=0.
                             else
                               sint=sqrt(sint)
                             end if
                  
                             phi=TWOPI*ran2(iseed)
                             cosp=cos(phi)
                             sinp=sin(phi)
                  
                  
                  
                                nxp = sint * cosp
                                nyp = sint * sinp
                                nzp = cost
                  
                  
                            !*************** Linear Grid *************************
                            xcell=int(nxg*(xp+xmax)/(2.*xmax))+1
                            ycell=int(nyg*(yp+ymax)/(2.*ymax))+1
                            zcell=int(nzg*(zp+zmax)/(2.*zmax))+1
                            !*****************************************************
                  
                  
                  
                        end subroutine fibre_lille


        real function ranu(a, b, iseed)

            implicit none


            real, intent(IN)       :: a, b
            integer, intent(INOUT) :: iseed

            real :: ran2

            ranu = a + ran2(iseed) * (b - a)

        end function ranu
end module sourceph_mod
