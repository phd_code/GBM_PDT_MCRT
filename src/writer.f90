module writer_mod

implicit none

    contains
        subroutine writer(xmax, ymax, zmax, nphotons, numproc, ts)

            use constants, only : nxg,nyg,nzg,fileplace
            use iarray,    only : jmeanGLOBAL, rhokap, S0_slice, o21_slice,o23_slice, &
            tumkill_slice,percent_left, power, tumour, O2_1, ua_ar, balloon, albedoar

            implicit none

            integer           :: nphotons, i, u, numproc, j,k, ts
            real              :: xmax, ymax, zmax, jm(nzg)

           

            inquire(iolength=i)jmeanGLOBAL

            open(newunit=u,file=trim(fileplace)//'jmean/jmean_run74NB.dat',access='stream',status='REPLACE',form='unformatted')
            write(u) jmeanGLOBAL
            close(u)
            open(newunit=u,file=trim(fileplace)//'jmean/abs_array.dat',access='stream',status='REPLACE',form='unformatted')
            write(u) ua_ar
            close(u)

            open(newunit=u,file=trim(fileplace)//'jmean/albedo_run74NB.dat',access='stream',status='REPLACE',form='unformatted')
            write(u) albedoar
            close(u)
            !open(newunit=u,file=trim(fileplace)//'jmean/balloon_test.dat',access='stream',status='REPLACE',form='unformatted')
            !write(u) balloon
            !close(u)

           ! open(newunit=u,file=trim(fileplace)//'jmean/tumour.dat',access='stream',status='REPLACE',form='unformatted')
           ! write(u) tumour
           ! close(u)

           ! open(newunit=u,file=trim(fileplace)//'jmean/o21_full_run11NB.dat',access='stream',status='REPLACE',form='unformatted')
            !write(u) O2_1
            !close(u)



            open(newunit=u,file=trim(fileplace)//'jmean/rhokap_run74NB.dat',access='stream',status='REPLACE',form='unformatted')
            write(u) rhokap
            close(u)

            open(newunit=u,file=trim(fileplace)//'jmean/tumkill_slice_run74NB.dat'&
            ,access='stream',status='REPLACE',form='unformatted')
            write(u) tumkill_slice
            close(u)

            open(newunit=u,file=trim(fileplace)//'jmean/percent_left_run74NB.dat'&
            ,access='stream',status='REPLACE',form='unformatted')
            write(u) percent_left
            close(u)

            open(newunit=u,file=trim(fileplace)//'jmean/s0_slice_run74NB.dat',access='stream',status='REPLACE',form='unformatted')
            write(u) S0_slice
            close(u)

            open(newunit=u,file=trim(fileplace)//'jmean/o23_slice_run74NB.dat',access='stream',status='REPLACE',form='unformatted')
            write(u) o23_slice
            close(u)

            open(newunit=u,file=trim(fileplace)//'jmean/o21_slice_run74NB.dat',access='stream',status='REPLACE',form='unformatted')
            write(u) o21_slice
            close(u)

            open(newunit=u,file=trim(fileplace)//'jmean/power_run74NB.dat',access='stream',status='REPLACE',form='unformatted')
            write(u) power
            close(u)

            jm = 0.d0
            do k = 1, nzg
                do j = 1, nyg 
                    do i = 1, nxg
                        jm(k) = jm(k) + jmeanGLOBAL(i,j,k)
                    end do
                end do
            end do

            jm = jm / (nxg*nyg)

            open(newunit=u,file=trim(fileplace)//"jmean/validation_run74NB.dat",status="replace")
           ! do i = nzg,1,-1
             do i= 1,nzg
                write(u,*)real(nzg-i)*(2.*zmax/nzg),jm(i)
            end do
            close(u)
        end subroutine writer
end module writer_mod
